=== Agency Street ===
Contributors: ThemeArile
Author: ThemeArile
Requires at least: 4.7
Tested up to: 6.0
Requires PHP: 5.6
Stable tag: 1.4
Version: 1.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: one-column, two-columns ,right-sidebar, flexible-header, custom-background, custom-header, custom-menu, editor-style, featured-images, footer-widgets, post-formats, theme-options, threaded-comments, rtl-language-support, translation-ready, full-width-template, custom-logo, blog, e-commerce, portfolio

== Description ==

Agency Street is a powerful, professional, and feature-rich multipurpose business WordPress theme. It comes with a pixel-perfect design and an outstanding customizer setting that you can easily customize. As well is sophisticated plus it has some exotic features like clean code, advanced typography, sticky menu, logo upload, header image, Bootstrap 4 framework, built with SEO in mind, and theme info area besides being Mobile-ready, translation ready (WPML, Polylang) and it's built to be beautiful on all screen sizes. The theme is perfectly suited for business, consultant, finance, corporate, freelancer, agency, business promotion, electrician, industries, education, SEO, construction, fashion, online shop, health & medical, Beauty & spa salons, wedding, photography, gym, architecture, lawyer, restaurant, hotel, blog, magazine, travel agency and many other websites compatible. This theme supports the best Elementor page builder to create, edit, and update page designs as per the requirement. Agency Street also supports popular free and premium WordPress plugins such as Elementor, Yoast SEO, WooCommerce, Contact Form 7, Jetpack, Google Analytics, and much more. Check the demo of ArileWP Pro https://themearile.com/arilewp-pro-theme/.

== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Type in Agency Street in the search form and press the 'Enter' key on your keyboard.
3. Click on the 'Activate' button to use your new theme right away.

== Copyright ==

Agency Street WordPress Theme, Copyright (c) 2019, ThemeArile
Agency Street is distributed under the terms of the GNU GPLs

== Credits ==

* Underscores - (C) 2012-2017 Automattic, Inc. - http://underscores.me/
License: GPLv2 or later (https://www.gnu.org/licenses/gpl-2.0.html) 

* Font Awesome by @davegandy - http://fontawesome.io/
License: (Font: SIL OFL 1.1, CSS: MIT License)

* Open Sans Font by Steve Matteson - https://www.google.com/fonts/specimen/Open+Sans
License: Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)

* Source Sans Pro Font by Paul D. Hunt - https://fonts.google.com/specimen/Source+Sans+Pro
License: Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)

* Animate.css - Copyright (c) 2018 Daniel Eden - http://daneden.me/animate
License: MIT (http://opensource.org/licenses/MIT)

* WOW - by Matthieu Aussaguel - mynameismatthieu.com
License: GNU GPL license v3

* Owl Carousel, (C) 2013 - 2018, David Deutsch
Source: https://github.com/OwlCarousel2/OwlCarousel2/
License: [MIT](http://opensource.org/licenses/MIT)

* SmartMenus - Copyright (c) Vasil Dinkov, Vadikom Web Ltd. - http://www.smartmenus.org/
License: MIT (http://opensource.org/licenses/MIT)

* Image for Screenshot Image, Credit CC0 Public Domain
  License: CC0 1.0 Universal (CC0 1.0)
  License URL: https://pxhere.com/en/license
  Source: https://pxhere.com/en/photo/171245
  
* Image for Testimonial Background Image, Credit CC0 Public Domain
  License: CC0 1.0 Universal (CC0 1.0)
  License URL: https://pxhere.com/en/license
  Source: https://pxhere.com/en/photo/682055

== Changelog ==

= Version 1.4
* Fixed the footer-sidebar section spacing issue.

= Version 1.3
* Updated footer copyright text.

= Version 1.2
* Removed the unnecessary left space for the wp-block-latest-posts in the sidebar widget.

= Version 1.1
* Added the menu active color in the screenshot.

= Version 1.0
* Initial release.