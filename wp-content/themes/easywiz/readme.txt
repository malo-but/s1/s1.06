=== EasyWiz ===
Contributors: DesertThemes
Requires at least: 4.7
Tested up to: 6.0
Requires PHP: 5.6
Stable tag: 0.9
License: GPLv3 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.html
Tags: one-column, two-columns ,right-sidebar, flexible-header, custom-background, custom-header, custom-menu, editor-style, featured-images, footer-widgets, post-formats, theme-options, threaded-comments, translation-ready, full-width-template, custom-logo, blog, e-commerce, portfolio


== Description ==

EasyWiz is the perfect theme for your website. EasyWiz is lightweight and highly extendable, it will enable you to create almost any type of website such a blog, portfolio, business, consultant, finance, corporate, freelancer, agency, business promotion, electrician, industries, education, SEO, construction, fashion, online shop, health & medical, Beauty & spa salons, wedding, photography, gym, cafe, music, architecture, lawyer, restaurant, hotel, blog, magazine, travel agency, RTL & Translation Ready and many other websites with a beautiful & professional design. EasyWiz supports popular WordPress plugins such as Elementor, WPML, Polylang, Yoast SEO, WooCommerce, Contact Form 7, Jetpack, Google Analytics, and much more. EasyWiz Pro demo https://preview.desertthemes.com/pro/easywiz/


== Installation ==
	
1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Type in EasyWiz in the search form and press the 'Enter' key on your keyboard.
3. Click on the 'Activate' button to use your new theme right away.


== Copyright ==

EasyWiz WordPress Theme, Copyright 2022 , Desert Themes
EasyWiz is distributed under the terms of the GNU GPL

Cosmobit WordPress Theme, Desert Themes. Cosmobit WordPress Theme is distributed under the terms of the GNU GPL



== Credits ==

* Underscores - (C) 2012-2017 Automattic, Inc. - http://underscores.me/
License: GPLv2 or later (https://www.gnu.org/licenses/gpl-2.0.html) 

* Font Awesome 4.7.0 by @davegandy - http://fontawesome.io - @fontawesome
Licenses - Font: SIL OFL 1.1, CSS: MIT License(http://fontawesome.io/license)
Source: http://fontawesome.io

* Owl Carousel 2.3.4 - by @David Deutsch
Licenses - MIT License(https://github.com/OwlCarousel2/OwlCarousel2/blob/master/LICENSE)
Source: https://github.com/OwlCarousel2

* WOW - by Thomas Grainger
Licenses - MIT License (https://github.com/graingert/WOW/blob/master/LICENSE)
Source: https://wowjs.uk/

* CounterUp - by Jeremy Paris
Licenses - GNU General Public License v2.0 (https://github.com/bfintal/Counter-Up/blob/master/LICENSE)
Source: https://github.com/bfintal/Counter-Up

* WayPoint - by Caleb Troughton
Licenses - MIT License (https://github.com/imakewebthings/waypoints/blob/master/licenses.txt)
Source: https://github.com/imakewebthings/waypoints/

* Animate Css by @Daniel Eden - http://daneden.me/animate
Licenses - MIT License(https://github.com/daneden/animate.css/blob/master/LICENSE)
Source: https://github.com/daneden/animate.css

* Customizer Controls 
Licenses - MIT license  (https://github.com/Codeinwp/customizer-controls/blob/master/LICENSE)
Source: https://github.com/Codeinwp/customizer-controls

* WP Bootstrap Navwalker
Licenses - GNU GENERAL PUBLIC LICENSE (https://github.com/wp-bootstrap/wp-bootstrap-navwalker/blob/master/LICENSE.txt)
Source: https://github.com/wp-bootstrap/wp-bootstrap-navwalker

* Sainitization
Licenses - GNU General Public License (https://github.com/WPTRT/code-examples/blob/master/LICENSE)
Source: https://github.com/WPTRT/code-examples


* Screenshot Banner Image - https://pxhere.com/en/photo/710494
License: Creative Commons Zero

* Screenshot Information Image - https://pxhere.com/en/photo/1453707 , https://pxhere.com/en/photo/1444157 , https://pxhere.com/en/photo/1432441
License: Creative Commons Zero

* Page Title Image - https://pxhere.com/en/photo/1456381
License: Creative Commons Zero

== Changelog ==

@version 0.9
* Fixed - Block Button Style Issue

@version 0.8
* Style improvements - Block Text

@version 0.7
* Style improvements - Block Video Caption

@version 0.6
* Style improvements - Block Image Caption

@version 0.5
* Style improvements - Block Outline Button

@version 0.4
* Removed - Unused CSS

@version 0.3
* Added Theme URI
* Added Upsale Link in Customizer

@version 0.2
* Fixed - Review Issues

@version 0.1
* Initial release